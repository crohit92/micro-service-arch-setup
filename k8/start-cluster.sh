#!/bin/bash
kubectl apply -f ./auth-provider-service/auth-provider-micro-service-db.yaml
kubectl apply -f ./auth-provider-service/auth-provider-micro-service.yaml
kubectl apply -f ./hello-world-service/hello-world-service.yaml
kubectl create secret generic auth-public-key --from-file=public.pem=./gateway/config/public.pem
kubectl apply -f ./gateway/gateway.yml