module.exports = (router) => {
    router.get('/:name', (req, res) => {
        res.json({ message: `Hello ${req.params.name}` })
    });
};
