const express = require('express');
const app = express();
const bodyParser = require('body-parser');
require('dotenv').config();
const controllers = require('./controllers');

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

app.use('/api', controllers);

app.listen(80, (err) => {
    if (!err) {
        console.log('Server started on 80');
    } else {
        console.log(err);
    }
})
