const User = require('../../models/user');
const fs = require('fs');
const md5 = require('md5');
const jwt = require('jsonwebtoken');
module.exports = (router) => {
    // router.get('/', (req, res) => {
    //     User.find({ deletedAt: null }, (err, users) => {
    //         if (err) {
    //             res.status(400).json(err);
    //         } else {
    //             res.json(users);
    //         }
    //     })
    // });

    router.post('/signin', (req, res) => {
        const credentials = req.body;
        User.findOne({ username: credentials.username, deletedAt: null }, (err, user) => {
            if (user) {
                const hashedPassword = md5(credentials.password);
                if (hashedPassword !== user.password) {
                    throw "Invalid credentials";
                } else {
                    const privateKey = fs.readFileSync(`${__dirname}/../../config/private.pem`);
                    res.json({
                        username: user.username,
                        token: jwt.sign(
                            { user: user.toObject() },
                            privateKey,
                            {
                                expiresIn: '8h',
                                algorithm: 'RS256'
                            }
                        )
                    })
                }
            } else {
                throw "Invalid credentials";
            }
        });

    });

    router.post('/', (req, res) => {
        const user = req.body;
        if (user.password) {
            user.password = md5(user.password);
            User.create(user, (err, _user) => {
                if (err) {
                    res.status(400).json(err);
                } else {
                    res.json({ _id: _user._id });
                }
            })
        }
    });
};
