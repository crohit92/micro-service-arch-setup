const { lstatSync, readdirSync } = require('fs');

const { join } = require('path');
const express = require('express');

const router = express.Router();
/**
 * Check if the path specified is of a directory or not
 * @param source Path of the file/directory to check if it is a directory
 */
const isDirectory = (source) => lstatSync(source).isDirectory();

/**
 * Get a segment of the route
 * @returns 
 * Returns on of the followinf possible values
 * * `Directory name` if it does not start with an _
 * * `:Directory name` if it starts with an _
 */
const lastSegmentFromPath = (dirPath) => {
    const segments = dirPath.split('/');
    const lastRouteSegment = segments[segments.length - 1];
    if (lastRouteSegment.startsWith('_')) {
        return `:${lastRouteSegment.split('_')[1]}`;
    } else {
        return lastRouteSegment;
    }
};

/**
 * List content of a directory
 * @param source Path of the directory whose content needs to be listed
 */
const ls = (source) =>
    readdirSync(source).map(name => {
        const path = join(source, name);
        return {
            path,
            isDirectory: isDirectory(path)
        }
    });

/**
 * Builds the routes for a directory and add those routes as children of the
 * parent router 
 * @param parentRouter parent router object
 * @param source Path of file/directory which needs to be added to the `parentRouter`
 */
const buildRouterForDir = (parentRouter, source) => {
    /**
     * Current route segment
     */
    const routeSegment = lastSegmentFromPath(source);
    /**
     * List all content in the source
     */
    ls(`${source}`)
        /**
         * sort them such that, parameters appear after
         * non-parameter route segments.
         * i.e. 
         * ```bash
         * ├── src
         * │   ├── controllers
         * │   │   ├── categories
         * │   │   │    ├── _id
         * │   │   │    │    └── products
         * │   │   │    │         └── get-products.ts 
         * │   │   │    └── subcategories        
         * │   │   │         └── products
         * │   │   │              └── get-products.ts 
         * │   │   └── integrations
         * │   │        └── freshdesk
         * │   │             └── index.ts 
         * │   └── Others
         * └── Others
         * ```
         * In the above directory structure
         * 
         * `/categories/subcategories/products` should appear before than
         * `/categories/:id/products`
         */
        .sort((f1, f2) => {
            const file1Name = lastSegmentFromPath(f1.path);
            const file2Name = lastSegmentFromPath(f2.path);
            if (!f1.isDirectory && !f2.isDirectory) {
                return file1Name < file2Name ? -1 : 1;
            } else if (!f1.isDirectory) {
                return -1;
            } else if (!f2.isDirectory) {
                return 1;
            } else if (file1Name.startsWith(':')) {
                return 1;
            } else {
                return -1;
            }
        }).forEach(async (file) => {
            const childRouter = express.Router({
                mergeParams: true
            });
            if (!isDirectory(file.path)) {
                if (file.path.match(/^.*\.js$/)) {
                    const childRouteFactory = require(file.path);
                    childRouteFactory(childRouter);

                }
            } else {
                buildRouterForDir(childRouter, file.path);
            }
            parentRouter.use(`/${routeSegment}`, childRouter);

        });
};
const childDirs = ls(__dirname).filter(file => isDirectory(file.path));
childDirs.forEach(dir => {
    buildRouterForDir(router, dir.path);
});

module.exports = router;