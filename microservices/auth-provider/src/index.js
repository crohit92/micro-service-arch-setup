const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
require('dotenv').config();
const controllers = require('./controllers');

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

app.use('/api', controllers);
mongoose.connect(process.env.DB_CONN, {
    useNewUrlParser: true
}, (err) => {
    if (err) {
        console.log(err);
        return;
    }
    app.listen(80, (err) => {
        if (!err) {
            console.log('Server started on 80');
        } else {
            console.log(err);
        }
    })
})
