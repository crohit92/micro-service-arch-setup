#!/bin/sh
set -e
echo "$@"
if [ "$1" = 'node*' ]; then
    if [ -z "$DB_CONN" ]; then
        DB_CONN=mongodb://localhost:27017/todo-app
    fi
    echo "DB_CONN=$DB_CONN">/usr/src/app/.env
fi

exec "$@"